
open Lwt_utils

let chain_id = "main"
let root = Format.sprintf "chains/%s/blocks" chain_id

let rec request ?(index=0) ?last_error path =
  let> nodes = Config.get_nodes () in
  let> verbose = Config.get_verbose () in
  if index >= List.length nodes then
    let e = match last_error with
    | None -> Lwt.return_error "Error.CrawlerError"
    | Some e -> e in
    e
    (* Format.ksprintf Lwt.return_error "request %S failed on all nodes / %s" path *)
  else
    let EzAPI.TYPES.BASE node = List.nth nodes index in
    let full_path = Format.sprintf "%s/%s/%s" node root path in
    let url = EzAPI.TYPES.URL full_path in
    let> r = EzCurl_lwt.get url in
    match r with
    | Ok s ->
      if verbose > 0 then Format.printf "> req %S\n%!" full_path;
      return (Ok s)
    | Error (code, content) ->
      let err = Format.sprintf "Request to %s failed with %d%s" path code
          (match content with None -> "" | Some s -> ": " ^ s) in
      Format.eprintf "%s@." err;
      request
        ~index:(index+1)
        ~last_error:(Lwt.return_error err) (* (Error.CrawlerRequestError (code, content)) *)
        path

let shell hash =
  let@! s = request (hash ^ "/header/shell") in
  EzEncoding.destruct Dune.Encoding.Header.shell_encoding s

let block ?(first=false) block_id =
  let@! s = request block_id in
  let encoding =
    if first then Dune.Encoding.Block.genesis_encoding
    else Dune.Encoding.Block.encoding in
  EzEncoding.destruct encoding s

let head_hash () =
  let@! s = request "head/hash" in
  String.sub s 1 (String.length s - 3)

let current_level () =
  let@! s = request "head/helpers/current_level" in
  EzEncoding.destruct Dune.Encoding.Level.encoding s
