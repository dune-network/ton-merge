
open Lwt_utils

let () =
  Arg.parse [] (fun s -> raise @@ Arg.Bad ("Extra argument " ^ s))
    "Dune TON merge crawler\nUsage:\n\tton-merge-crawler";
  Lwt_main.run @@
  let> () = Crawler.init () in
  Crawler.loop ()
