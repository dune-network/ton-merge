(**************************************************************************)
(*                                                                        *)
(*    Copyright 2021 OCamlPro                                             *)
(*                                                                        *)
(*  All rights reserved. This file is distributed under the terms of the  *)
(*  GNU Lesser General Public License version 2.1, with the special       *)
(*  exception on linking described in the file LICENSE.                   *)
(*                                                                        *)
(**************************************************************************)

open Lwt_utils
open Data_types

let start_daemon ~only_freeton () =

  let _keypair = match Freeton.keypair with
    | None -> failwith "You must provide a passpharse with TON_MERGE_PASSPHRASE"
    | Some keypair -> keypair
  in

  let _ =
    if only_freeton then
      ()
    else
      try ignore ( Sys.getenv "DUNE_INJECTOR_SK" ) with
      | _ ->
        failwith "You must provide the Dune key with DUNE_INJECTOR_SK"
  in

  let max_role_index = 8 in (* 5 + 3 *)
  let monitors = Array.make max_role_index None in

  Sys.set_signal Sys.sigint (Sys.Signal_handle (fun signal ->
      Array.iter (function
          | None -> () | Some pid ->
            (try Unix.kill pid signal with _ -> ());
        ) monitors ;
      exit 2
    ));

  let string_of_status = function
    | Unix.WEXITED n -> Printf.sprintf "exited %d" n
    | Unix.WSIGNALED n -> Printf.sprintf "signaled %d" n
    | Unix.WSTOPPED n -> Printf.sprintf "stopped %d" n
  in
  let rec iter config =

    let bindir = Filename.dirname Sys.argv.(0) in

    let f ~cmd ~name ~args ~ref =
      let restart () =
        Printf.eprintf "Starting relay %s \n\n%!" name;
        let exe = Filename.concat bindir cmd in
        let pid = Unix.create_process
            exe
            ( Array.of_list ( exe :: args ) )
            Unix.stdin Unix.stdout Unix.stderr
        in
        monitors.( ref ) <- Some pid;
        Lwt.return_unit
      in
      let> old_pid = Db.PID.get name in
      match old_pid, monitors. ( ref ) with
      | None, Some _old_pid ->
        Printf.eprintf "Process %s died. Restarting\n%!" name;
        restart ()
      | None, None ->
        Printf.eprintf "Starting process %s\n%!" name;
        restart ()
      | Some pid, None ->
        Printf.eprintf
          "Process %s under pid %d is already running\n%!" name pid;
        Lwt.return_unit
      | Some _, Some _ ->
        Lwt.return_unit
    in

    let> () =
      f
        ~cmd:"ton-merge-freeton"
        ~name:"monitor_freeton"
        ~args: [ "--monitor-freeton" ]
        ~ref:0
    in

    let fr role =
      let name = "monitor_database" ^ Monitor_database.ROLE.to_suffix role in
      let role_name = Monitor_database.ROLE.to_string role in
      f ~cmd:"ton-merge-freeton"
        ~name
        ~args: [ "--monitor-database"; role_name ]
        ~ref: ( Monitor_database.ROLE.index role )
    in
    let> () =
      let> () =  fr RoleDeployer in
      let> () =  fr RoleConfirmer in
      let> () =  fr RoleRevealer in
      let> () =  fr RoleFinisher in
      Lwt.return_unit
    in

    let> max_daemons =
      if only_freeton then
        Lwt.return 4
      else
        let> () =
          f
            ~cmd:"ton-merge-api-server"
            ~name:"api_server"
            ~args: []
            ~ref:5
        in

        let> () =
          f
            ~cmd:"ton-merge-dune-crawler"
            ~name:"dune_crawler"
            ~args: []
            ~ref:6
        in

        let> () =
          f
            ~cmd:"ton-merge-dune-injector"
            ~name:"dune_injector"
            ~args: []
            ~ref:7
        in
        Lwt.return 7
    in

    for i = 0 to max_daemons do
      match monitors.( i ) with
      | None ->
        Printf.eprintf
          "Fatal error: monitor %d was already running...\n%!" i;
        exit 2
      | Some _ -> ()
    done;

    let> res = Lwt_unix.wait () in
    match res with
    | exception exn ->
      Printf.eprintf "Exception: %s\n%!" (Printexc.to_string exn);
      exit 2
    | ( pid, status ) ->
      Printf.eprintf "monitor with pid %d died with status %s\n%!" pid
        (string_of_status status);
      Printf.eprintf "Waiting 5 seconds before restart\n%!";
      let> () = Lwt_unix.sleep 5. in
      iter config
  in
  Lwt_main.run (

    let> old_pid = Db.PID.get "monitor" in
    match old_pid with
    | Some pid ->
      Printf.eprintf "Monitor is already running with pid %d\n%!" pid;
      exit 2
    | _ ->
      let> config = Config.get_config () in
      let _keypair = Monitor_database.check config in
      let> () = Db.PID.register "monitor" in
      iter config )
