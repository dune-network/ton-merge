/* Interface IRootUserWallet */

pragma ton-solidity >= 0.37.0;

interface IDuneVesting {

  function vest( address multisig, address depool, bool for6months) external ;
  function collect( address multisig ) external ;
  
  function getCollectInfo() external
    returns (uint64 collect_date1 ,
             uint64 collect_date2 ,
             uint128 amount,
             uint128 bonus
             );

   }
