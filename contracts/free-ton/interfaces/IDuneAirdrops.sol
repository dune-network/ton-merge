/* Interface IRootUserWallet */

pragma ton-solidity >= 0.37.0;

interface IDuneAirdrops {

  function getVestingAddress( uint256 pubkey ) external view returns (address);
  function deployVestingContract( uint256 pubkey ) external view returns (address);
}
