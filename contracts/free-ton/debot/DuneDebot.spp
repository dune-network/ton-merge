pragma ton-solidity >=0.35.0;

#include "lib/cpp.sol"

pragma AbiHeader expire;
pragma AbiHeader time;
pragma AbiHeader pubkey;

import "lib/Debot.sol";
import "lib/Terminal.sol";
import "lib/AddressInput.sol";
import "lib/AmountInput.sol";
import "lib/ConfirmInput.sol";
import "lib/Sdk.sol";
import "lib/Menu.sol";
import "lib/Upgradable.sol";
import "lib/Transferable.sol";

import "lib/Utility.sol";
import "../interfaces/IMultisig.sol";
import "../interfaces/IDuneRootSwap.sol";
import "../interfaces/IDuneVesting.sol";
import "../interfaces/IDePool.sol";



contract DuneDebot is Debot, Upgradable, Transferable, Utility {

  DEBOT_NAME("DuneDebot",
        "OCamlPro",
        "Debot for Dune holders to vest their TON tokens",
        "Fabrice LE FESSANT");
  DEBOT_VERSION(1,0,0);
  // your address with 0x instead of 0:
  DEBOT_SUPPORT(
                0x66e01d6df5a8d7677d9ab2daf7f258f1e2a7fe73da5320300395f99e01dc3b5f );
  
  DEBOT_HELLO(
    "Hi, I will help to manage your new TON tokens"
              );

  uint8 constant EXN_WRONG_ARGUMENT = 101 ;

  struct DePoolInfo {
    string name ;
    address addr ;
  }
  
  mapping( uint256 => DePoolInfo ) g_depools;
  
  function setDePools( string[] names, address[] addresses ) public
  {
    require( names.length == addresses.length, EXN_WRONG_ARGUMENT );
    ACCEPT_TVM_PUBKEY();
    mapping( uint256 => DePoolInfo ) map;
    for( uint i = 0 ; i < names.length ; i++ ){
      map[i] =  DePoolInfo( names[i] , addresses[i] );
    }
    g_depools = map ;
  }

  TvmCell g_surfCode ;
  function setSurfCode( TvmCell code ) public
  {
    require( tvm.hash(code) == CODEHASH(SetcodeMultisigWallet2),
             EXN_WRONG_ARGUMENT );
    ACCEPT_TVM_PUBKEY();
    g_surfCode = code;
  }
  
  function setRootContract( address root ) public
  {
    require(msg.pubkey() == tvm.pubkey(), 100);
    tvm.accept();
    g_root = root ;
  }


  /****************************************************************

                           USER INTERFACE

 ******************************************************************/
  
  function start() public override {
    // TODO
    PRINT("");
    PRINT("This is DuneDebot.");
    _askMultisigAddress();
  }

  QUERY_CONTRACT_FUNCTION( _askMultisigAddress,
                           "What is your account address ?",
                           g_multisig,
                           _onHasMultisigAddress();
                           )


  function _onHasMultisigAddress() internal
  {
    PRINTFCC(reallyGetCustodians,
             "Getting my pubkey from account {}", g_multisig);
  }

  function reallyGetCustodians() public view
  {
    IMultisig( g_multisig ).getCustodians
      MSGINT(onHasCustodians,onErrorGetCustodians) ();
  }

  function onErrorGetCustodians(uint32 sdkError, uint32 exitCode) public {
    PRINTF("Error: sdkError:{} exitCode:{}", sdkError, exitCode);
    PRINT("Is your multisig active ? Check on https://ton.surf that your multisig has a non-empty balance and try a small transfer.");
    _askMultisigAddress();
  }
  
  uint256 g_pubkey;
  
  function onHasCustodians( CustodianInfo[] custodians ) public
  {
    if( custodians.length != 1 ){
      PRINT("Multisig has more than one custodian. You should either create a new Surf multisig, or you will need to confirm all transactions with other signatures during this process.");
      _askPubkey();
    } else {
      g_pubkey = custodians[0].pubkey;
      PRINTFCC( onHasPubkey, "Working with pubkey: 0x{:x}", g_pubkey);
    }
  }

  QUERY_HEXA_FUNCTION( _askPubkey, "Enter your pubkey:", pubkey,
                       g_pubkey = pubkey ;
                       PRINTFCC( onHasPubkey,
                                 "Working with pubkey: 0x{:x}", g_pubkey);
                       )
  
  address g_root ;
  
  address g_vesting_addr ;
  function onHasPubkey() public view
  {
    IDuneRootSwap( g_root ).getVestingAddress
      MSGINT( onVestingAddr, onErrorRestart) ( g_pubkey );
  }
  
  function onVestingAddr( address vesting_addr ) public
  {
    g_vesting_addr = vesting_addr ;
    PRINTFCC(checkVestingContractBalanceCC,
             "Vesting contract: {}", vesting_addr );
  }

  function checkVestingContractBalanceCC() public
  {
    Sdk.getBalance(
                       F_(checkVestingContractBalance),
                       g_vesting_addr);
  }

  uint128 g_vesting_balance;
  function checkVestingContractBalance( uint128 nanotokens )
  {
    g_vesting_balance = nanotokens ;
    if( g_vesting_balance == 0 ){
      PRINT("Empty balance. Already emptied or wrong pubkey ?");
      start();
    } else {
      PRINTFCC(checkVestingContractStatusCC,
               "Vesting contract balance: {}", tonsToStr( nanotokens ));
    }
  }
    
  function checkVestingContractStatusCC() public
  {
    Sdk.getAccountType(
                       F_(checkVestingContractStatus),
                       g_vesting_addr);

  }

  function checkVestingContractStatus( uint8 acc_type )
  {
    PRINTF("Vesting contract status: {}", acc_type);
    if( acc_type == 1 ){
      mainMenu();
    } else {
      PRINTF("Contract {} not yet deployed.", g_vesting_addr);
      MENU(
           MENU_ITEM( "Try deploy Vesting contract", deployVestingContract );
           MENU_ITEM( "Check status again",
                      checkVestingContractBalanceCC);
           );
    }
  }

  function deployVestingContract() public 
  {
    PRINTCC(reallyDeployVestingContract,
            "Deploying vesting contract with 2 tons from multisig");
  }

  function reallyDeployVestingContract() public view
  {
    TvmCell payload = tvm.encodeBody( IDuneRootSwap.deployVestingContract,
                                      g_pubkey );
    IMultisig( g_multisig ).sendTransaction
      MSGEXT(g_pubkey, checkVestingContractBalanceCC,onErrorRestart)
    (g_root, 2 ton, true, 0, payload);
  }

  function mainMenu() public view
  {
    IDuneVesting( g_vesting_addr ).getCollectInfo
      MSGINT( onCollectInfo, onErrorRestart ) ();
  }

  uint64 g_collect_date1;
  uint64 g_collect_date2;
  uint128 g_amount ;
  uint128 g_bonus ;
  function onCollectInfo( uint64 collect_date1,
                          uint64 collect_date2,
                          uint128 amount,
                          uint128 bonus ) public
  {
    g_collect_date1 = collect_date1 ;
    g_collect_date2 = collect_date2 ;
    g_amount = amount ;
    g_bonus = bonus ;
    _mainMenu();
  }

  function _mainMenu() public
  {
    if( g_amount == 0 ){
      PRINT("Funds already collected. Nothing to do here.");
      start();
    } else {
      PRINTF("Vesting contract amount: {}", tonsToStr( g_amount ));
      PRINTF("Maximal bonus: {}", tonsToStr( g_bonus ));
      string delay1 = _computeDelay( g_collect_date1 );
      string delay2 = _computeDelay( g_collect_date2 );
      PRINTF("Delay before direct collect with minimal bonus: {}", delay1);
      PRINTF("Delay before direct collect with maximal bonus: {}", delay2);
      MENU(
           MENU_ITEM("Vest tokens", vestTokens);
           if( delay1 == "passed" ){
             MENU_ITEM("Collect tokens", collectTokens);
           }
           MENU_ITEM("Check status again", mainMenu);
           );    
    }
  }

  function onErrorMenu(uint32 sdkError, uint32 exitCode) public {
    PRINTF("Error: sdkError:{} exitCode:{}", sdkError, exitCode);
    mainMenu();
  }
    
  function vestTokens() public
  {
    _queryDePoolAddress() ;
  }

  function _queryDePoolAddress() public
  {
    PRINT("Choose a DePool");
    for( (uint num, DePoolInfo depool): g_depools ){
      PRINTF("{} : {} at {}", num, depool.name, depool.addr );
    }
    PRINT("or nothing to enter a different DePool address");
    INPUT(onQueryDepoolAddress, "Your choice:", false);
  }

  function onQueryDepoolAddress( string value ) public
  {
    if( value == "" ){
      _askDePoolAddress();
    } else {
      (uint256 v, bool res) = stoi( value );
      if( !res ){
        _queryDePoolAddress();
      } else {
        optional( DePoolInfo ) opt = g_depools.fetch( v );
        if( opt.hasValue() ){
          g_depool = opt.get().addr ;
          _getDePoolStatus();
        } else {
          PRINT("Invalid choice");
          _queryDePoolAddress();
        }
      }
    }
  }

  QUERY_CONTRACT_FUNCTION( _askDePoolAddress,
                           "Enter DePool address:",
                           g_depool,
                           _getDePoolStatus();
                           )

  function _getDePoolStatus() internal
  {
    PRINTFCC(_getDePoolStatusCC, "Calling getDePoolInfo on {}", g_depool);
  }
  
  function _getDePoolStatusCC() public view
  {
    IDePool( g_depool ).getDePoolInfo
      MSGINT2( onDePoolInfo, onErrorMenu) ();
  }

  uint64 g_minStake ;
  uint64 g_stakeFee ;
  function onDePoolInfo
     (
      bool poolClosed
      , uint64 minStake
      , uint64 // validatorAssurance
      , uint8 // participantRewardFraction
      , uint8 validatorRewardFraction
      , uint64 // balanceThreshold
      
      , address // validatorWallet
      , address[] // proxies
      
      , uint64 stakeFee
      , uint64 // retOrReinvFee
      , uint64 // proxyFee
      ) public
  {
    if( poolClosed ){
      PRINT("Error: DePool is closed. Try another one.");
      _queryDePoolAddress();
    } else {
      PRINT("DePool Information:");
      PRINTF("  Min stake: {} TON", tonsToStr(minStake));
      PRINTF("  Fees: {}%", validatorRewardFraction);
      g_minStake = minStake ;
      g_stakeFee = stakeFee ;
      CONFIRM(confirmDepool, "Do you confirm this DePool ?");
    }
  }

  function confirmDepool( bool value ) public view
  {
    if( value ) {
      checkParticipant();
    } else {
      mainMenu();
    }
  }

  function checkParticipant() public view
  {
    IDePool( g_depool).getParticipants
      MSGINT2( onParticipants, onErrorMenu ) ();
  }

  function onParticipants( address[] participants ) public
  {
    bool is_participant = false ;
    for( address participant : participants ){
      if( participant == g_multisig ){
        is_participant = true ;
        break ;
      }
    }
    if( is_participant ){
      PRINTCC(checkParticipantInfo, "Your multisig is registered as a participant");
    } else {
      PRINT("Your multisig is not yet registered as a participant");
      MENU(
           MENU_ITEM( "Register multisig with minimal stake",
                      becomeParticipant );
           MENU_ITEM( "Check again", checkParticipant );
           MENU_ITEM( "Go back to main menu", mainMenu );
           );
    }
  }
  
  function checkParticipantInfo() public view
  {
    IDePool( g_depool).getParticipantInfo
      MSGINT2( onParticipantInfo, onErrorMenu ) ( g_multisig );
  }
  
  function onParticipantInfo(
                             uint64 // total
                             , uint64 // withdrawValue
                             , bool // reinvest
                             , uint64 // reward
                             , mapping (uint64 => uint64) // stakes
                             , mapping (uint64 => IDePool.InvestParams) vestings
                             , mapping (uint64 => IDePool.InvestParams) // locks
                             , address vestingDonor
                             , address // lockDonor
                             ) public
  {
    if( !vestings.empty() ){
      PRINT("Error: this DePool has already vesting for this multisig.");
      PRINT("Choose another depool or create another multisig.");
      mainMenu();
    } else {
      if( vestingDonor != g_vesting_addr ){
        PRINTCC(menuSetVestingDonor, "You must set the vesting donor");
      } else {
        PRINT("You are ready to vest all your tokens:");
        CONFIRM( setVestingPeriod, "vest for the maximal period ?" );
      }
    }
  }

  function menuSetVestingDonor() public
  {
    MENU(
         MENU_ITEM("Set vesting donor now", setVestingDonor);
         MENU_ITEM("Check again", checkParticipantInfo);
         );
  }
  
  function setVestingDonor() public view
  {
    TvmCell payload = tvm.encodeBody( IDePool.setVestingDonor,
                                          g_vesting_addr );
    IMultisig( g_multisig ).sendTransaction
      MSGEXT(g_pubkey, checkParticipant, onErrorMenu)
      (g_depool, g_stakeFee + 0.1 ton , true, 0, payload);
  }
  
  bool g_for6months ;
  function setVestingPeriod( bool value ) public
  {
    g_for6months = value ;
    PRINTCC(reallyVestTokens, "Vesting tokens...");
  }

  function reallyVestTokens() public view
  {
    IDuneVesting( g_vesting_addr ).vest
      MSGEXT( g_pubkey, onVestedTokens, onErrorMenu )
      ( g_multisig, g_depool, g_for6months );
  }

  function onVestedTokens() public
  {
    PRINTCC(mainMenu, "Tokens vested !");
  }
  
  function becomeParticipant() public
  {
    Sdk.getBalance( _F( onMultisigBalance), g_multisig );
  }

  uint128 g_multisig_balance ;
  function onMultisigBalance( uint128 nanotokens ) public
  {
    g_multisig_balance = nanotokens ;
    uint128 need = g_minStake + g_stakeFee + 0.1 ton ;
    if( nanotokens < need ){
      PRINTF("You multisig balance of {} ton is not enough ({} ton needed)",
             tonsToStr(g_multisig_balance),
             tonsToStr(need));
      PRINT("Send more tons to multisig, or choose another DePool");
      mainMenu();
    } else {
      CONFIRM( onConfirmOrdinaryStake,
               format("Send {} stake to DePool to become participant ?",
                      tonsToStr(need) ) );
    }
  }

  function onConfirmOrdinaryStake( bool value ) public
  {
    if( value ){
      TvmCell payload = tvm.encodeBody( IDePool.addOrdinaryStake,
                                        g_minStake );
      IMultisig( g_multisig ).sendTransaction
        MSGEXT(g_pubkey, checkParticipant, onErrorMenu)
        (g_depool, g_minStake + g_stakeFee + 0.1 ton , true, 0, payload);
    } else {
      PRINT("Aborted.");
      mainMenu();
    }
  }
  
  function collectTokens() public
  {
    if( now < g_collect_date2 ){
      string delay = _computeDelay( g_collect_date2 );
      PRINTF("Delay before maximal bonus collect: {}", delay);
      CONFIRM( onCollectTokensConfirm ,
                        "Collect with minimal bonus only ?");
    } else {
      onCollectTokensConfirm(true);
    }
  }

  function onCollectTokensConfirm( bool value ) public
  {
    if( value ){
      PRINTCC(reallyCollectTokens, "Sending collect message...");
    } else {
      mainMenu();
    }
  }

  function reallyCollectTokens() public view
  {
    IDuneVesting( g_vesting_addr ).collect
      MSGEXT( g_pubkey, onCollectTokensOk, onCollectTokensError )
      ( g_multisig );
  }

  function onCollectTokensOk() public
  {
    PRINT("Tokens sent to multisig. Check your account !");
    start();
  }

  function onCollectTokensError(uint32 sdkError, uint32 exitCode) public
  {
    PRINTF("Error: sdkError={} exitCode={}", sdkError, exitCode );
    PRINT("Something weird happened.");
    mainMenu();
  }

#include "lib/StdMethods.sol"
}
