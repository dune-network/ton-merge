/*
  Implementation of contract DePool: a fake depool to be able to test...
 */

pragma ton-solidity >= 0.37.0;

pragma AbiHeader expire;
pragma AbiHeader pubkey;

import "../interfaces/IDePool.sol";
import "../interfaces/IParticipant.sol";

contract FakeDePool is IDePool {

  uint8 constant EXN_AUTH_FAILED = 100 ;
  uint8 constant EXN_NO_SENDER = 101 ;


  uint64 constant DEPOOL_FEE = 0.5 ton ;
  uint64 constant MIN_STAKE = 100 ton;

  address g_donor ;
  address g_participant ;
  uint32  n_errors ;
  uint128 g_vesting_stake ;
  uint32  g_withdrawalPeriod ;
  uint32  g_totalPeriod ;
  uint128 g_stake ;
  
  constructor() public
    {
      require( tvm.pubkey() == msg.pubkey() , EXN_AUTH_FAILED );
      tvm.accept();
    }

  function _sendError(uint32 errcode, uint64 comment) private pure {
    IParticipant(msg.sender).receiveAnswer{value:0, bounce: false, flag: 64}(errcode, comment);
  }

  uint8 constant STATUS_NO_PARTICIPANT = 6;
  uint8 constant STATUS_STAKE_TOO_SMALL = 1;
  uint8 constant STATUS_PARTICIPANT_ALREADY_HAS_VESTING = 9;
  uint8 constant STATUS_TOTAL_PERIOD_IS_NOT_DIVISIBLE_BY_WITHDRAWAL_PERIOD = 13;
  uint8 constant STATUS_FEE_TOO_SMALL = 21;
  uint8 constant STATUS_INVALID_DONOR = 23;

  // @dev Vest some stake for benefiary. Calls back the sender
  //    receiveAnswer method in case of success, or error (returning
  //    the stake) The benefiary must have declared the sender as a
  //    donor beforehand, using the setVestingDonor() method. ALso, a
  //    fee of 0.5 TON is expected
  // @param stake The total skate in nanoton
  // @param benefiary The address that should received unlocked tokens
  // @param withdrawalPeriod The time between incremental unvesting
  // @param totalPeriod      The total time of the vesting
  function addVestingStake(  uint64  stake
                           , address beneficiary
                           , uint32  withdrawalPeriod
                           , uint32  totalPeriod
                           )
    public override
  {
    require( msg.sender != address(0), EXN_NO_SENDER );

    if( beneficiary != g_participant ){
      return _sendError( STATUS_NO_PARTICIPANT, 0 );
    } 

    if( msg.sender != g_donor ){
      return _sendError( STATUS_INVALID_DONOR, 0 );
    }
    
    if( msg.value < stake + DEPOOL_FEE ){
      return _sendError( STATUS_FEE_TOO_SMALL, 0 );
    }

    if( stake < MIN_STAKE ){
      return _sendError( STATUS_STAKE_TOO_SMALL, 0 );
    }

    if (totalPeriod % withdrawalPeriod != 0) {
      return _sendError(STATUS_TOTAL_PERIOD_IS_NOT_DIVISIBLE_BY_WITHDRAWAL_PERIOD, 0);
    }

    if( g_vesting_stake > 0 ){
      return _sendError( STATUS_PARTICIPANT_ALREADY_HAS_VESTING, 0);
    }
    
    g_vesting_stake = stake ;
    g_stake += stake ;
    g_withdrawalPeriod = withdrawalPeriod ;
    g_totalPeriod = totalPeriod ;
    IParticipant( g_donor ).receiveAnswer
      { value: msg.value, bounce: false, flag : 64 } ( 1, 0 );

  }

  // @dev Declare a new donor for the sender, prior to the use of
  //    addVestingStake()
  // @param donor The address of the donor
  function setVestingDonor(address donor) public override
  {
    require( msg.sender != address(0), EXN_NO_SENDER );

    if( g_participant != msg.sender ){
      return _sendError( STATUS_NO_PARTICIPANT, 0 );
    }
    g_donor = donor ;
  }

  function addOrdinaryStake(uint64 stake) public override
  {
    
    if( stake < MIN_STAKE ){
      return _sendError( STATUS_STAKE_TOO_SMALL, 0 );
    }

    g_stake += stake ;
    g_participant = msg.sender ;
  }

  
  function getDePoolInfo() public view override returns (
        bool poolClosed,
        uint64 minStake,
        uint64 validatorAssurance,
        uint8 participantRewardFraction,
        uint8 validatorRewardFraction,
        uint64 balanceThreshold,

        address validatorWallet,
        address[] , // proxies

        uint64 stakeFee,
        uint64 retOrReinvFee,
        uint64 proxyFee
    )
  {
    poolClosed = false ;
    minStake = MIN_STAKE;
    validatorAssurance = 1000 ton;
    participantRewardFraction = 95 ;
    validatorRewardFraction = 5 ;
    balanceThreshold = 10 ton ;
    validatorWallet = address(1);
    stakeFee = DEPOOL_FEE ;
    retOrReinvFee = 40 milliton ;
    proxyFee = 0.09 ton ;
  }

  function getParticipants() public view override
    returns (address[] participants)
  {
    if( g_participant.value != 0 ){
      participants.push( g_participant );
    }
  }

    function getParticipantInfo(address addr) public override view
        returns (
            uint64 total,
            uint64 withdrawValue,
            bool reinvest,
            uint64 reward,
            mapping (uint64 => uint64) stakes,
            mapping (uint64 => InvestParams) vestings,
            mapping (uint64 => InvestParams) locks,
            address vestingDonor,
            address lockDonor
        )
    {
      // TODO
    }
}

