#!/bin/bash

set -euo pipefail

. ./env.sh

$FT switch to testnet

$FT account create test1
$FT account set test1 --surf

$FT account create depool1 --address 0:268864dfa2abb35976d8ab2ccd5f359f02143bb36f2f9cdcf770f2ec1a3e2c76 --contract DePool -f
$FT account create depool2 --address 0:ed30d6f116ad62b02cae15925467f29bd2d07f6b89f324765d8fd14dd2a8f500 --contract DePool -f
$FT account create depool3 --address 0:3bde61cac478ded8aed9e7ff5679cb418a8c9534355c2b674b87344e3621623a --contract DePool -f


$FT multisig transfer 110 --from $DEPLOYER --to $TESTUSER --sponsor

$FT multisig create $TESTUSER
