#!/bin/bash

set -euo pipefail

. ./env.sh

$FT contract deploy DuneDebot --sign $DEPLOYER --deployer $DEPLOYER --credit 1 '{}' -f

$FT call DuneDebot setABI --sign $DEPLOYER '{ "dabi": "%{hex:read:contract:abi:DuneDebot}" }'

$FT call DuneDebot setIcon --sign $DEPLOYER '{ "icon": "%{hex:string:data:image/png;base64,%{base64:file:DuneDebot.png}}" }'

$FT call DuneDebot setRootContract --sign $DEPLOYER '{ "root": "%{account:address:root_address}" }'

$FT call DuneDebot setDePools --sign $DEPLOYER '{ "names": [ "%{hex:string:Vigorous Hellman}" , "%{hex:string:Distracted Ptolemy}", "%{hex:string:Kind Black}"], "addresses": [ "%{account:address:depool1}", "%{account:address:depool2}", "%{account:address:depool3}" ] }'

$FT call DuneDebot setSurfCode --sign $DEPLOYER '{ "code": "%{get-code:contract:tvc:SetcodeMultisigWallet2}"}'





