#!/bin/bash

set -euo pipefail

. ./env.sh

# Use this script with a modified version of tonos-cli for local caching of debots

$FT output 'TESTUSER address: %{account:address:%{env:TESTUSER}}'
$FT output 'TESTUSER pubkey: 0x%{account:pubkey:%{env:TESTUSER}}'
$FT output 'TESTUSER passphrase: %{account:passphrase:%{env:TESTUSER}}'

DEBOT_LOAD_DIR=$HOME/.ft/debots $FT client -- debot fetch %{net-account:sandbox1:address:DuneDebot}
