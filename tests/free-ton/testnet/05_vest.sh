#!/bin/bash

set -euo pipefail

. ./env.sh

$FT call vest_addr --sign $TESTUSER vest '{ "multisig": "%{account:address:%{env:TESTUSER}}", "depool": "%{account:address:depool3}", "for6months": true }' --wait


