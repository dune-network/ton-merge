#!/bin/bash

. ./env.sh

$FT account remove dune_airdrops

$FT contract deploy DuneAirdrops --create dune_airdrops \
    --credit 50 \
    --sign admin \
    '{ "giver_addr" : "%{account:address:freeton_giver}", "batch_size": 100, "duneVestingCode": "%{get-code:contract:tvc:DuneVesting2}","duneAirdropCode": "%{get-code:contract:tvc:DuneAirdrop}" }' || exit 2
