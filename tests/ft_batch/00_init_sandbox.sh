#!/bin/bash

echo -n sandbox1 > NETWORK

. ./env.sh
unset FT_SWITCH

$FT switch to mainnet || exit 2

FT_SWITCH=${NETWORK}

rm -f *.block *.calc

killall ft

$FT --switch ${NETWORK} node stop
$FT switch remove ${NETWORK}
$FT switch create ${NETWORK} || exit 2

export FT_SWITCH

$FT node start || exit 2
echo
echo Waiting 10 seconds for warmup
sleep 10

export FTGIVER_PASSPHRASE="dolphin mansion aunt water put wealth crisp invite smoke nasty myth lens"

$FT account create freeton_giver --surf --passphrase "%{env:FTGIVER_PASSPHRASE}" || exit 2
$FT node give freeton_giver --amount ${GIVER_TONS} || exit 2


export ADMIN_PASSPHRASE="race hospital smart require radar sunny trim bike receive fashion disease unable"

$FT account create admin --surf --passphrase "%{env:ADMIN_PASSPHRASE}" || exit 2
$FT multisig transfer 100 --from freeton_giver --to admin --parrain || exit 2
$FT multisig create admin || exit 2

$FT config --deployer admin || exit 2
