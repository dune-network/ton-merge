#!/bin/bash

. ./env.sh

$FT call dune_airdrops deployBatch --sign admin --subst @%{res:addr} --output batch.addr || exit 2

$FT account remove batch

$FT account create batch --address $(cat batch.addr) --contract DuneAirdrop || exit 2
