#!/bin/bash

set -euo pipefail

. ./env.sh

DEPLOYER=admin
export DUNE_DEBOT_CONTRACT=DuneDebot2

$FT contract deploy $DUNE_DEBOT_CONTRACT --create DuneDebot --sign $DEPLOYER --deployer $DEPLOYER --credit 1 '{}' -f

$FT call DuneDebot setABI --sign $DEPLOYER '{ "dabi": "%{hex:read:contract:abi:%{env:DUNE_DEBOT_CONTRACT}}" }'

$FT call DuneDebot setIcon --sign $DEPLOYER '{ "icon": "%{hex:string:data:image/png;base64,%{base64:file:DuneDebot.png}}" }'

$FT call DuneDebot setRootContract --sign $DEPLOYER '{ "root": "%{account:address:dune_airdrops}" }'

$FT call DuneDebot setAirdropContract --sign $DEPLOYER '{ "root": "%{account:address:dune_airdrops}" }'

#$FT call DuneDebot setDePools --sign $DEPLOYER '{ "names": [ "%{hex:string:Vigorous Hellman}" , "%{hex:string:Distracted Ptolemy}", "%{hex:string:Kind Black}"], "addresses": [ "%{account:address:depool1}", "%{account:address:depool2}", "%{account:address:depool3}" ] }'

$FT call DuneDebot setSurfCode --sign $DEPLOYER '{ "code": "%{get-code:contract:tvc:SetcodeMultisigWallet2}"}'

$FT account info admin -S   
$FT debot fetch DuneDebot





