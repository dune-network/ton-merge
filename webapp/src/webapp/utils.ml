open Ezjs_tyxml

open Html

let debug = ref true

let output_debug s = if !debug then
    Format.printf "[debug] %s\n" s

let placeholder ?(classes=[]) ~id () = div ~a:[a_id id; a_class ("placeholder" :: classes)] []

let add_text_to_placeholder id t =
  match Manip.by_id id with
    None -> failwith "better error mesg"
  | Some s -> Manip.replaceChildren s [txt t]

let img_path = "imgs"

let get_arg arg_name args =
  try
    Some (List.assoc arg_name args)
  with Not_found -> None

let link ?(args=[]) path =
  match args with
  | [] -> path
  | _ ->
    let args =
      String.concat "&"
        (List.map (fun (key, value) -> key ^ "=" ^ value) args)
    in
    if String.contains path '?' then
      Printf.sprintf "%s&%s" path args
    else
      Printf.sprintf "%s?%s" path args

let dispatcher :
  (path:string -> args:(string * string) list -> unit) ref
  = ref
    begin
      fun
        ~(path : string)
        ~(args : (string * string) list) : unit ->
        ignore path ; ignore args ;
        log "Misc_ui.dispatcher was not set.";
        ()
    end

let dispatch ~path ~args =
  Dom_html.window##.history##pushState
    (Js.some (Js.string path))
    (Js.string "")
    (Js.some (Js.string path));
  Ezjs_loc.set_args args;
  !dispatcher ~path ~args

let a_link ?(args=[]) ?(classes=[]) ~path content =
  a ~a:[a_href (link ~args path); a_class classes] content

let is_none = function None -> true | Some _ -> false

let get_value id =
  match Manip.by_id id with
    None -> ""
  | Some elt -> Manip.value elt

let hash_password str = Hashtbl.hash str

let show_id id =
  match Manip.by_id id with
    None -> ()
  | Some elt -> show elt

let hide_id id =
  match Manip.by_id id with
    None -> ()
  | Some elt -> hide elt

let to_dun str =
  match float_of_string_opt str with
    None -> "nan"
  | Some f -> (string_of_float (f /. 1000000.)) ^ "đ"
