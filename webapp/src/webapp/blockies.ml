open Js_of_ocaml
open Js
open Vue_component

module Base58 = struct
  let alphabet = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

  let charcodes =
    let h = Hashtbl.create @@ String.length alphabet in
    String.iteri (fun i c -> Hashtbl.add h c i) alphabet;
    h

  let b58_code c =
    match Hashtbl.find_opt charcodes c with
    | None -> failwith "Not base58"
    | Some i -> i

  let base58_to_int s =
    let n = String.length s in
    if n <= 0 then Z.zero
    else
      let rec aux = function
        | 0 -> Z.one, Z.of_int (b58_code s.[n-1])
        | i ->
          let (pos, x) = aux (i-1) in
          let y = Z.of_int (b58_code s.[n-i-1]) in
          let pos = Z.mul pos (Z.of_int 58) in
          (pos, Z.add x (Z.mul y pos)) in
      snd (aux (n-1))

  let big_int_to_hex b =
    let b16 = Z.of_int 16 in
    let rec aux b =
      if b > Z.zero then
        let q, r = Z.div_rem b b16 in
        Printf.sprintf "%s%x" (aux q) (Z.to_int r)
      else "" in
    let s = aux b in
    if (String.length s) mod 2 = 0 then s else "0" ^ s

  let to_hex s =
    s |> base58_to_int |> big_int_to_hex
end

let convdef f x = match Optdef.to_option x with
  | None -> undefined
  | Some x -> def (f x)

let init () =
  Vue_component.make
    ~render:Render.blockies_render
    ~static_renders:Render.blockies_static_renders
    ~props:(PrsArray [ "seed"; "color"; "bgcolor"; "size"; "scale"; "spotcolor" ])
    ~computed:(Mjs.L ["dataurl", (fun app ->
        let options = object%js
          val seed = convdef (fun s -> string @@ Base58.to_hex (to_string s)) app##.seed
          val color = app##.color
          val bgcolor = app##.bgcolor
          val size = app##.size
          val scale = app##.scale
          val spotcolor = (* app##.spotcolor *) def @@ string "#000"
        end in
        let icon = (Ezjs_blockies.blockies ())##create options in
        def @@ Mjs.to_any icon##toDataURL)])
    "v-blockies" |> ignore
